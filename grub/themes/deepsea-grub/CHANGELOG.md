2015/12/09
v0.1
+ Added Devuan-specific logo, background and OS selection icons.
+ Added README.md and CHANGELOG files.
