Devuan Grub Theme
=================

This theme is intended for the 1.0 release of Devuan linux.

It was adapted from the Vimix theme by vinceliuice at <http://vinceliuice.deviantart.com>.

Copyright Dyne.org, License GPLv2.

Please see CHANGELOG.md for the revision history.


