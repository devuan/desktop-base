# README for desktop-base

This package contains the artwork for the Devuan which is used by
default for the desktop installations.  It provides backgrounds,
bootloader backgrounds and themes, and splash themes.

Releases Build status:

Experimental: [![Build Status](http://ci.devuan.org/view/suites-999-experimental/job/desktop-base-binaries/badge/icon)](http://ci.devuan.org/view/suites-999-experimental/job/desktop-base-binaries/)


1. Images
   1.1. Emblems
   1.2. Pixmaps
   1.3. Splash and Wallpapers
   1.4. Boot splash
2. Desktop files
3. Changing desktop-base images
4. Window Managers
5. Boot splash

## 1. Images

### 1.1. Emblems

  /usr/share/icons/hicolor/48x48/emblems/emblem-devuan.png
  /usr/share/icons/hicolor/48x48/emblems/emblem-devuan.icon

### 1.2. Pixmaps

  /usr/share/pixmaps/devuan-security.png

### 1.3. Splash, wallpapers and grub images

Wallpapers are provided in two SVG variants, one standard ('narrow')
aspect (4/3) and one widescreen ('wide') (16/9).  Both come in 'small'
and 'large' formats to accommodate various display sizes.  You can
switch from one another using the alternatives system.  Using the same
system you can select one of the other theme variant.  Beware that
changing alternative means that the next package upgrade won't
automatically revert to the default choice.

For wallpaper you can run:

```
sudo update-alternatives --config desktop-background
```

For splash screens you can run:

```
sudo update-alternatives --config desktop-splash
```

For grub, you can select standard or widescreen version too.
'wide-large' can be better in particular for UEFI firmwares:

```
sudo update-alternatives --config desktop-grub
```

## 2. Desktop files

```
/usr/share/desktop-base/devuan-homepage.desktop
/usr/share/desktop-base/devuan-reference.desktop
/usr/share/desktop-base/devuan-security.desktop
```

## 3. Changing desktop-base images

**Note:** If you'd like to propose different desktop backgrounds,
you're welcome to share it on [Friends of Devuan].

We need professional looking Devuan artwork for the Desktop. If you
want to help changing some `desktop-base` images, please consider the
following notes:

- Each release has a _signature color_.
  - For JESSIE that is _purpy_ and it is `#7b7691`.
  - For ASCII that is _darkpurpy_ 
  - In reserve is _leafy_ and it is `#b5d147`.
  - Next releases are not settled yet.  There's a palette in
    [devuan-art.git].
- The Devuan font is Open Sans (fortunately a free font)

If you have suggestions for different artwork, please join the
`#devuan-art` channel on Freenode and fork [devuan-art.git] to create
a merge request.  See our [Devuan Style Guide].

[devuan-art.git]: https://git.devuan.org/devuan-editors/devuan-art.git
[Devuan Style Guide]: https://git.devuan.org/devuan-editors/devuan-art/wikis/home
[Friends of Devuan]: http://wiki.friendsofdevuan.org/doku.php/community_artwork

## 4. Window Managers

`desktop-base` is used by KDE/LXDE/Mate/XFCE desktop environments.

If you're responsible for a WM in Devuan and want to use
`desktop-base` artwork or add your own image (wallpaper, splash,
whatever) use the layout described above and send your patch through
[`devuan-art`](https://git.devuan.org/devuan-editors/devuan-art/issues)
issue tracker.

## 5. Boot splash

`desktop-base` ships several Plymouth themes. To activate the default
theme, install the `plymouth` and `plymouth-themes` packages and run:

```
plymouth-set-default-theme lines
```

Then remember to add “splash” to your kernel command line.

To do that permamently, edit `/etc/default/grub` and add `splash` to
the `GRUB_CMDLINE_LINUX_DEFAULT` variable. For example:

```
GRUB_CMDLINE_LINUX_DEFAULT="quiet splash"
```

You may also keep the grub background and hide the kernel boot
messages with:

```
GRUB_CMDLINE_LINUX_DEFAULT="quiet splash fbcon=vc:2"
```
